package base;

import enums.Role;
import io.restassured.response.Response;
import lombok.Data;
import model.requestModel.LoginRequestModel;
import org.testng.annotations.AfterMethod;
import steps.Steps;

@Data
public class makeMeTopBaseTest implements Steps {
    Response response;
    public String accessToken;
    public String refreshToken;

    public void login(String login, String password, Role role){
        response = AUTH_STEPS.sendLoginRequest(LoginRequestModel.builder().login(login).password(password).role(role).build());
        accessToken = response.body().jsonPath().getString("accessToken.accessToken");
        refreshToken = response.body().jsonPath().getString("refreshToken.refreshToken");;
    }

    @AfterMethod
    public void logout(){AUTH_STEPS.sendLogoutRequest(refreshToken);}
}
