package config;

import org.aeonbits.owner.Config;

@org.aeonbits.owner.Config.Sources(value = "classpath:db.properties")
public interface DbConfig extends Config {

    @Key("baseURL")
    String baseUri();
    @Key("username")
    String username();
    @Key("password")
    String password();
    @Key("connection_string")
    String connectionString();
    @Key("db_name")
    String dataBaseName();
}
