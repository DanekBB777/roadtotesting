package config;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

import static io.restassured.http.ContentType.ANY;
import static io.restassured.http.ContentType.JSON;

public class Specification {

    static DbConfig config = org.aeonbits.owner.ConfigFactory.create(DbConfig.class);

    public static RequestSpecification requestSpecification(){
      return new RequestSpecBuilder()
              .setBaseUri(config.baseUri())
              .setContentType(JSON)
              .build();
    }


    public static ResponseSpecification responseSpecification(Integer code){
        return new ResponseSpecBuilder()
                .expectStatusCode(code)
                .expectContentType("application/json")
                .build();
    }
}
