package enums;

public enum CommonErrors {
    ERROR_UNAUTHORIZED("Вам закрыт доступ к данной функциональности бортового компьютера"),
    ERROR_FORBIDDEN("Вам закрыт доступ к данной функциональности бортового компьютера"),
    ERROR_BAD_REQUEST("Ошибка в поступивших данных"),;

    private String description;

    CommonErrors(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}