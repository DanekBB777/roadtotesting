package model.requestModel;


import enums.Role;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginRequestModel {
    public String login;
    public String password;
    public Role role;
}
