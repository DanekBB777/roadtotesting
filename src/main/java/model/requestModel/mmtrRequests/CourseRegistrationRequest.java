package model.requestModel.mmtrRequests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseRegistrationRequest {
    public Long courseId;
    public List<Integer> keeperIds;
}
