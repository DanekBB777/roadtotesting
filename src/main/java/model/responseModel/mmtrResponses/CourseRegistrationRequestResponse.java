package model.responseModel.mmtrResponses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseRegistrationRequestResponse {
    public Integer courseId;
    public Integer personId;
    public String requestDate;
    public Integer statusId;
}
