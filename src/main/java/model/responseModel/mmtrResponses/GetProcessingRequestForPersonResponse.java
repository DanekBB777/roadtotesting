package model.responseModel.mmtrResponses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProcessingRequestForPersonResponse {
    private Long requestId;
    private Long courseId;
    private Long personId;
    private String requestDate;
    private Long statusId;
}
