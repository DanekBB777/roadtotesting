package steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import model.requestModel.LoginRequestModel;

import static config.Specification.requestSpecification;
import static config.Specification.responseSpecification;
import static io.restassured.RestAssured.*;

public class AuthSteps {

    @Step
    public Response sendLoginRequest(LoginRequestModel loginRequestModel){
        return given().when().spec(requestSpecification()).log().all()
                .body(loginRequestModel)
                .post("auth/login")
                .then().log().all()
                .extract().response();
    }


    @Step
    public Response sendLogoutRequest(String token){
        return given().when()
                .spec(requestSpecification())
                .body(token)
                .post("/auth/logout")
                .then().spec(responseSpecification(200)).log().all()
                .extract().response();
    }
}
