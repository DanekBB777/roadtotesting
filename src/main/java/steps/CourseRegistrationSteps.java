package steps;

import io.qameta.allure.Step;
import model.responseModel.mmtrResponses.ApproveCourseRequestResponse;
import model.responseModel.mmtrResponses.CancelCourseRequestResponse;
import model.responseModel.mmtrResponses.GetProcessingRequestForPersonResponse;
import model.requestModel.RejectRequest;
import model.requestModel.mmtrRequests.CourseRegistrationRequest;

import static config.Specification.requestSpecification;
import static io.restassured.RestAssured.*;

public class CourseRegistrationSteps {

    @Step
    public Long sendCourseRegistrationRequest(CourseRegistrationRequest request, String token) {
        return given().log().all()
                .headers("Authorization", "Bearer " + token)
                .when()
                .spec(requestSpecification())
                .body(request)
                .post("course-registration-app/course-requests")
                .then().statusCode(201).log().all()
                .extract().body().as(Long.class);
    }

    @Step
    public GetProcessingRequestForPersonResponse getProcessingRequestForPersonResponse(String token) {
        return given().log().all()
                .headers("Authorization", "Bearer " + token)
                .when()
                .spec(requestSpecification())
                .get("course-registration-app/course-requests/processing")
                .then().statusCode(200).log().all()
                .extract().body().as(GetProcessingRequestForPersonResponse.class);
    }

    @Step
    public ApproveCourseRequestResponse approveCourseRequest(Long requestId, String token) {
        return given().log().all()
                .headers("Authorization", "Bearer " + token)
                .when()
                .spec(requestSpecification())
                .patch("course-registration-app/course-requests/" + requestId)
                .then().statusCode(200).log().all()
                .extract().body().as(ApproveCourseRequestResponse.class);
    }

    @Step
    public CancelCourseRequestResponse cancelRequestOnEducation(Long requestId, String token) {
        return given().log().all()
                .headers("Authorization", "Bearer " + token)
                .when()
                .spec(requestSpecification())
                .delete("course-registration-app/course-requests/" + requestId)
                .then().statusCode(200).log().all()
                .extract().body().as(CancelCourseRequestResponse.class);
    }

    @Step
    public void rejectCourseRequest(Long requestId, String token, RejectRequest reject) {
        given().log().all().spec(requestSpecification())
                .headers("Authorization", "Bearer " + token)
                .when()
                .body(reject)
                .post("course-registration-app/course-requests/+"+requestId+"/rejections")
                .then().statusCode(201).log().all();
    }
}
