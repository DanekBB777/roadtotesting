package steps;

import useless.UserListSteps;
import useless.UserPasswordSteps;

public interface Steps {
    UserPasswordSteps USER_PASSWORD_STEPS = new UserPasswordSteps();
    UserListSteps USER_LIST_STEPS = new UserListSteps();
    ReqresSteps REQRES_STEPS = new ReqresSteps();
    PetStoreSteps PET_STORE_STEPS = new PetStoreSteps();
    UiSteps UI_STEPS = new UiSteps();
    DbSteps DB_STEPS = new DbSteps();
    CourseRegistrationSteps COURSE_REGISTRATION_STEPS = new CourseRegistrationSteps();
    AuthSteps AUTH_STEPS = new AuthSteps();
}
