package makeMeTopTests;

import base.makeMeTopBaseTest;
import model.responseModel.mmtrResponses.ApproveCourseRequestResponse;
import model.responseModel.mmtrResponses.CancelCourseRequestResponse;
import model.responseModel.mmtrResponses.GetProcessingRequestForPersonResponse;
import model.requestModel.RejectRequest;
import model.requestModel.mmtrRequests.CourseRegistrationRequest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

import static enums.Role.EXPLORER;
import static enums.Role.KEEPER;

public class CourseRegistrationServiceTest extends makeMeTopBaseTest {

    @Test
    public void sendCourseRegistrationRequestTest() {
        login("user10", "user10", EXPLORER);

        List<Integer> keepersIds = List.of(6,12);
        CourseRegistrationRequest request = new CourseRegistrationRequest(6L, keepersIds);

        Long response = COURSE_REGISTRATION_STEPS.sendCourseRegistrationRequest(
                request, accessToken
        );

        GetProcessingRequestForPersonResponse getProcessingRequestForPersonResponse = COURSE_REGISTRATION_STEPS.
                getProcessingRequestForPersonResponse(accessToken);

        Assert.assertTrue(response.equals(getProcessingRequestForPersonResponse.getRequestId())
                && getProcessingRequestForPersonResponse.getCourseId().equals(request.courseId));
    }

    @Test
    public void startEducationOnCourseTest() {
        login("user10", "user10", EXPLORER);

        List<Integer> keepersIds = List.of(6,12);
        CourseRegistrationRequest request = new CourseRegistrationRequest(6L, keepersIds);

        Long response = COURSE_REGISTRATION_STEPS.sendCourseRegistrationRequest(
                request, accessToken
        );

        GetProcessingRequestForPersonResponse getProcessingRequestForPersonResponse = COURSE_REGISTRATION_STEPS.
                getProcessingRequestForPersonResponse(accessToken);

        Assert.assertTrue(response.equals(getProcessingRequestForPersonResponse.getRequestId())
                && getProcessingRequestForPersonResponse.getCourseId().equals(request.courseId));


        login("user", "user", KEEPER);

        ApproveCourseRequestResponse approveCourseRequestResponse =
                COURSE_REGISTRATION_STEPS.approveCourseRequest(getProcessingRequestForPersonResponse.getRequestId(), accessToken);

        Assert.assertTrue(approveCourseRequestResponse.getRequestId().equals(getProcessingRequestForPersonResponse.getRequestId())
        && approveCourseRequestResponse.statusId == 2);
    }

    @Test
    public void cancelRequestOnEducationTest() {
        login("user10", "user10", EXPLORER);

        List<Integer> keepersIds = List.of(6, 12);
        CourseRegistrationRequest request = new CourseRegistrationRequest(6L, keepersIds);

        Long response = COURSE_REGISTRATION_STEPS.sendCourseRegistrationRequest(
                request, accessToken
        );

        GetProcessingRequestForPersonResponse getProcessingRequestForPersonResponse = COURSE_REGISTRATION_STEPS.
                getProcessingRequestForPersonResponse(accessToken);

        Assert.assertTrue(response.equals(getProcessingRequestForPersonResponse.getRequestId())
                && getProcessingRequestForPersonResponse.getCourseId().equals(request.courseId));

        CancelCourseRequestResponse cancelCourseRequestResponse =
                COURSE_REGISTRATION_STEPS.cancelRequestOnEducation(getProcessingRequestForPersonResponse.getRequestId(), accessToken);

        Assert.assertEquals(cancelCourseRequestResponse.message, "Вы отменили запрос на прохождение курса 6"
                + getProcessingRequestForPersonResponse.getCourseId());
    }

    @Test
    public void rejectCourseRegistrationRequestTest(){
        login("user10", "user10", EXPLORER);

        List<Integer> keepersIds = List.of(6, 12);
        CourseRegistrationRequest request = new CourseRegistrationRequest(6L, keepersIds);

        Long response = COURSE_REGISTRATION_STEPS.sendCourseRegistrationRequest(
                request, accessToken
        );

        GetProcessingRequestForPersonResponse getProcessingRequestForPersonResponse = COURSE_REGISTRATION_STEPS.
                getProcessingRequestForPersonResponse(accessToken);

        Assert.assertTrue(response.equals(
                getProcessingRequestForPersonResponse.getRequestId())
                && getProcessingRequestForPersonResponse.getCourseId().equals(request.courseId));

        login("user", "user", KEEPER);

        COURSE_REGISTRATION_STEPS.rejectCourseRequest(
                getProcessingRequestForPersonResponse.getRequestId(),
                accessToken, new RejectRequest(2L));

    }
}
