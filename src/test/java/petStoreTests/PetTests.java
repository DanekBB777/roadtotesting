package petStoreTests;

import base.ApiBaseTest;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import model.requestModel.petStoreRequests.Category;
import model.requestModel.petStoreRequests.PetRequest;
import model.requestModel.petStoreRequests.Tag;
import model.responseModel.petStoreResponses.ApiResponse;
import model.responseModel.petStoreResponses.PetResponse;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

@Epic("Тесты сайта petStore")
@Feature("Группа тестов направленная на Pet")
public class PetTests extends ApiBaseTest {

    private static Long petId;

    @Test(testName = "Add Pet",priority = 1, description = "Создание питомца")
    public static void addNewPetTest() {
        Category category = new Category(0, "string");
        Tag tag = new Tag(0, "string");
        ArrayList<String> photoUrls = new ArrayList<>();
        photoUrls.add("string");
        ArrayList<Tag> tags = new ArrayList<>();
        tags.add(tag);
        PetRequest petRequest = new PetRequest(143, category, "doggie-corgi-super", photoUrls, tags, "available");
        PetResponse petResponse = PET_STORE_STEPS.postPetAdd(petRequest);
        petId = petResponse.id;
        Assert.assertEquals(petResponse.id, petId);
    }
    @Test(testName = "GET Pets By Status", description = "Получения списка питомцев по статусу")
    public static void findPetsByStatusTest() {
        List<PetResponse> petsList = PET_STORE_STEPS.getPetsByStatus("sold");
        Assert.assertFalse(petsList.isEmpty());
    }

    @Test(testName = "GET Pet By ID",description = "Найти питомца по ID",dependsOnMethods = {"addNewPetTest"})
    public static void findPetByIdTest() {
        PetResponse petResponse = PET_STORE_STEPS.getPetById(petId);
        Assert.assertEquals(petResponse.name, "doggie-corgi-super");
    }

    @Test(testName = "UPDATE Pet",description = "Обновление данных о питомце",dependsOnMethods = {"addNewPetTest"})
    public static void updatePetWithFormDataTest() {
        ApiResponse response = PET_STORE_STEPS.postPetUpdate(petId,"1doggie-corgi-super1" , "sold");
        Assert.assertTrue(response.code == 200 && response.message.equals(petId.toString()));
    }

    @Test(testName = "DELET Pet",description = "Удалить питомца",dependsOnMethods = {"updatePetWithFormDataTest"})
    public static void deletePetTest() {
        ApiResponse response = PET_STORE_STEPS.deletePet(petId);
        Assert.assertTrue(response.code == 200 && response.message.equals(petId.toString()));
    }

}
